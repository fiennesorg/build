package org.fiennes.ant.node;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.cord.node.config.NodeConfig;
import org.cord.node.config.NodeConfig.Namespace;
import org.cord.node.grid.GntmlGrid;
import org.cord.node.grid.Grid;
import org.cord.util.Gettable;
import org.cord.util.IoUtil;

public class GntmlGenerateGrid
  extends Task
{
  private String _docbase = "www";
  private String _file = null;

  public void setDocbase(String docbase)
  {
    _docbase = docbase;
  }

  public void setFile(String file)
  {
    _file = file;
  }

  private String debug(String type,
                       String value)
  {
    System.out.println(type + "=" + value);
    return value;
  }

  @Override
  public void execute()
      throws BuildException
  {
    try {
      NodeConfig nodeConfig = new NodeConfig(new File(_docbase));
      Gettable nodeGettable = nodeConfig.getGettable(Namespace.NODE);
      GntmlGrid gntmlGrid = new GntmlGrid(nodeGettable, null);
      gntmlGrid.addGntmlPlusBlockStyle("alert", 2d);
      gntmlGrid.addGntmlPlusBlockStyle("tint", 2d);
      String sql = gntmlGrid.generateSql();
      sql +=
          String.format("update RegionStyle set compilerStyleName = '%s' where compilerStyleName = 'contentComp';\n",
                        debug("mainCompDoc",
                              nodeGettable.getString(Grid.GET_HEADER + "mainCompDoc")));
      sql +=
          String.format("update RegionStyle set compilerStyleName = '%s' where compilerStyleName = 'contentOpt';\n",
                        debug("mainOptDoc",
                              nodeGettable.getString(Grid.GET_HEADER + "mainOptDoc")));
      sql +=
          String.format("update RegionStyle set compilerStyleName = '%s' where compilerStyleName = 'zoomComp';\n",
                        debug("zoomCompDoc",
                              nodeGettable.getString(Grid.GET_HEADER + "zoomCompDoc")));
      sql +=
          String.format("update RegionStyle set compilerStyleName = '%s' where compilerStyleName = 'zoomOpt';\n",
                        debug("zoomOptDoc",
                              nodeGettable.getString(Grid.GET_HEADER + "zoomOptDoc")));
      IoUtil.writeContent(new File(_file), sql);
    } catch (Exception e) {
      throw new BuildException(e);
    }
  }
  public static void main(String[] args)
  {
    GntmlGenerateGrid task = new GntmlGenerateGrid();
    task._docbase = args[1];
    task._file = args[2];
    task.execute();
  }
}
