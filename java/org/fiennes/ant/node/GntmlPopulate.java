package org.fiennes.ant.node;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.awtwf.gntml.Gntml;
import org.cord.node.NodeManager;
import org.fiennes.build.node.AppTools;
import org.fiennes.build.node.GntmlTools;

public class GntmlPopulate
  extends Task
{
  private String _classname = null;
  private String _docbase = "www";
  private String _content = null;

  public void setClassname(String classname)
  {
    _classname = classname;
  }

  public void setDocbase(String docbase)
  {
    _docbase = docbase;
  }

  public void setContent(String content)
  {
    _content = content;
  }

  private static void populateGntml(String appManagerClassName,
                                    String docBase,
                                    final String content)
      throws Exception
  {
    AppTools.withNodeManager(appManagerClassName, new File(docBase), true, new AppTools.Handler() {
      @Override
      public void run(NodeManager nodeManager,
                      String transactionPassword)
          throws Exception
      {
        Gntml gntmlCC =
            (Gntml) nodeManager.getContentCompilerFactory().getContentCompiler(Gntml.NAME);
        GntmlTools.populateDummyGntml(nodeManager.getDb(), transactionPassword, gntmlCC, content);
      }
    });
  }

  @Override
  public void execute()
      throws BuildException
  {
    try {
      populateGntml(_classname, _docbase, _content);
    } catch (Exception e) {
      throw new BuildException(e);
    }
  }

  public static void main(String[] args)
  {
    GntmlPopulate task = new GntmlPopulate();
    task._classname = args[0];
    task._docbase = args[1];
    task.execute();
  }
}
