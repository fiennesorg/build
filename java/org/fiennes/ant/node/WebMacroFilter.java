package org.fiennes.ant.node;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.apache.tools.ant.filters.BaseParamFilterReader;
import org.apache.tools.ant.filters.ChainableReader;
import org.apache.tools.ant.types.Parameter;
import org.apache.tools.ant.util.FileUtils;
import org.fiennes.build.node.WMUtils;

// Ant custom filters. Just say no. You can _tell_ no-one outside the ant
// team has ever written one of these...
public class WebMacroFilter
  extends BaseParamFilterReader
  implements ChainableReader
{
  public WebMacroFilter()
  {
    super();
  }

  public WebMacroFilter(Reader in)
  {
    super(in);
  }

  @Override
  public int read()
      throws IOException
  {
    initialize();
    return super.read();
  }

  public Reader chain(Reader in)
  {
    return new WebMacroFilter(in);
  }

  private void initialize()
  {
    if (!getInitialized()) {
      String output;
      try {
        Map<String, String> context = new HashMap<String, String>();
        for (Parameter p : getParameters()) {
          context.put(p.getName(), p.getValue());
        }
        output = WMUtils.eval(FileUtils.readFully(in), context, null);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
      this.in = new StringReader(output);
      setInitialized(true);
    }
  }
}
