package org.fiennes.ant.node;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.awtwf.gntml.Gntml;
import org.cord.node.NodeManager;
import org.fiennes.build.node.AppTools;
import org.fiennes.build.node.GntmlTools;

public class GntmlRegenerate
  extends Task
{
  private String _classname = null;
  private String _docbase = "www";

  public void setClassname(String classname)
  {
    _classname = classname;
  }

  public void setDocbase(String webxml)
  {
    _docbase = webxml;
  }

  private static void regenerateGntml(String appManagerClassName,
                                      String docBase)
      throws Exception
  {
    AppTools.withNodeManager(appManagerClassName, new File(docBase), true, new AppTools.Handler() {
      @Override
      public void run(NodeManager nodeManager,
                      String transactionPassword)
          throws Exception
      {
        Gntml gntmlCC =
            (Gntml) nodeManager.getContentCompilerFactory().getContentCompiler(Gntml.NAME);
        GntmlTools.regenerateGntml(nodeManager.getDb(), transactionPassword, gntmlCC);
      }
    });
  }

  @Override
  public void execute()
      throws BuildException
  {
    try {
      regenerateGntml(_classname, _docbase);
    } catch (Exception e) {
      throw new BuildException(e);
    }
  }
}
