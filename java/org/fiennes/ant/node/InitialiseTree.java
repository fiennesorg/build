package org.fiennes.ant.node;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.cord.node.NodeManager;
import org.fiennes.build.node.AppTools;
import org.fiennes.build.node.AppTools.Handler;

import com.google.common.base.Objects;

public class InitialiseTree
  extends Task
{
  public static final String ROOTNODE_HTMLTITLE = "RootNode.htmlTitle";
  public static final String ROOTNODE_TITLE = "RootNode.title";

  private String _classname = null;
  private String _docbase = "www";

  public void setClassname(String classname)
  {
    _classname = classname;
  }

  public void setDocbase(String docbase)
  {
    _docbase = docbase;
  }

  @Override
  public void execute()
      throws BuildException
  {
    try {
      AppTools.withNodeManager(_classname, new File(_docbase), false, new Handler() {
        @Override
        public void run(NodeManager nodeMgr,
                        String pwd)
            throws Exception
        {
          String htmlTitle =
              Objects.firstNonNull(nodeMgr.getGettable().getString(ROOTNODE_HTMLTITLE), "Home");
          String title =
              Objects.firstNonNull(nodeMgr.getGettable().getString(ROOTNODE_TITLE), "Home");
          nodeMgr.initialiseNodeTree(htmlTitle, title);
        }
      }, null);
    } catch (Exception e) {
      throw new BuildException(e);
    }
  }

  public static void main(String[] args)
  {
    InitialiseTree task = new InitialiseTree();
    task._classname = args[0];
    task._docbase = args[1];
    task.execute();
  }
}
