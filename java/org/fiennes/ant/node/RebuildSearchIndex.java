package org.fiennes.ant.node;

import java.io.File;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.cord.node.NodeManager;
import org.fiennes.build.node.AppTools;

public class RebuildSearchIndex
  extends Task
{
  private String _classname = null;
  private String _docbase = "www";

  public void setClassname(String classname)
  {
    _classname = classname;
  }

  public void setDocbase(String docbase)
  {
    _docbase = docbase;
  }

  private static void rebuildIndex(String appManagerClassName,
                                   String docBase)
      throws Exception
  {
    AppTools.withNodeManager(appManagerClassName, new File(docBase), true, new AppTools.Handler() {
      @Override
      public void run(NodeManager nodeManager,
                      String transactionPassword)
          throws Exception
      {
        nodeManager.getSearchMgr().rebuildIndex((org.cord.task.Task) null);
      }
    });
  }

  @Override
  public void execute()
      throws BuildException
  {
    try {
      rebuildIndex(_classname, _docbase);
    } catch (Exception e) {
      throw new BuildException(e);
    }
  }
}
