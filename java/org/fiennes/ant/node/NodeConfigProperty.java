package org.fiennes.ant.node;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Task;
import org.cord.node.config.NodeConfig;
import org.cord.util.Gettable;

public class NodeConfigProperty
  extends Task
{
  private ArrayList<String> _nodeProperties = new ArrayList<String>();
  private String _namespace = "node";
  private String _prefix = null;
  private String _docBase = "www";
  private String _default = null;
  private boolean _debug = false;

  public void setNodeProperty(String nodeProperty)
  {
    _nodeProperties.clear();
    _nodeProperties.add(nodeProperty);
  }

  public void addText(String text)
  {
    for (String prop : text.split("\n")) {
      prop = prop.trim();
      if (!prop.isEmpty()) {
        _nodeProperties.add(prop);
      }
    }
  }

  public void setPrefix(String prefix)
  {
    _prefix = prefix;
  }

  public void setDocBase(String docBase)
  {
    _docBase = docBase;
  }

  public void setNamespace(String namespace)
  {
    _namespace = namespace;
  }

  public void setDebug(boolean debug)
  {
    _debug = debug;
  }

  public void setDefault(String def)
  {
    _default = def;
  }

  private NodeConfig getNodeConfig()
  {
    return new NodeConfig(new File(_docBase), _debug ? new PrintWriter(System.err, true) : null);
  }

  @Override
  public void execute()
      throws BuildException
  {
    try {
      Gettable propTree = getNodeConfig().getGettable(_namespace);

      for (String nodeProperty : _nodeProperties) {
        String property = _namespace + "." + nodeProperty;
        if (_prefix != null)
          property = _prefix + "." + nodeProperty;

        String value = (String) propTree.get(nodeProperty.trim());
        if (value != null) {
          getProject().setNewProperty(property, value);
        } else if (_default != null) {
          getProject().setNewProperty(property, _default);
        }
      }

    } catch (Exception e) {
      throw new BuildException(e);
    }
  }
}
