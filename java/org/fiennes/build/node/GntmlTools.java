package org.fiennes.build.node;

import org.awtwf.gntml.Gntml;
import org.cord.mirror.Db;
import org.cord.mirror.PersistentRecord;
import org.cord.mirror.Table;
import org.cord.mirror.Transaction;
import org.cord.mirror.WritableRecord;
import org.cord.node.Node;
import org.cord.node.RegionStyle;
import org.cord.util.SettableMap;

public class GntmlTools
{
  // ==========================================================================
  // Utility methods

  private static void populateDummyGntml(Transaction transaction,
                                         WritableRecord gntmlInstance,
                                         Gntml gntmlCC,
                                         String content)
      throws Exception
  {
    if (!gntmlInstance.comp(Gntml.GNTML).isEmpty()) {
      return;
    }

    PersistentRecord node = gntmlInstance.comp(Gntml.NODE);
    PersistentRecord regionStyle = gntmlInstance.comp(Gntml.REGIONSTYLE);
    PersistentRecord gntmlStyle = gntmlInstance.comp(Gntml.STYLE);

    String title = regionStyle.comp(RegionStyle.TITLE);
    String name = regionStyle.comp(RegionStyle.NAME);
    String gntmlStyleName = regionStyle.comp(RegionStyle.COMPILERSTYLENAME);

    String gntml =
        String.format("**%s** \\\\(%s %s)\\\\\n\n", title, gntmlStyleName, name) + content;

    SettableMap params = new SettableMap();
    params.set("gntml", gntml);

    gntmlInstance.setField(Gntml.ISDEFINED, true);

    gntmlCC.updateTransientInstance(node,
                                    regionStyle,
                                    gntmlInstance,
                                    gntmlStyle,
                                    transaction,
                                    params,
                                    null,
                                    null);
  }

  private static void regenerateGntml(Transaction transaction,
                                      WritableRecord gntmlInstance,
                                      Gntml gntmlCC)
      throws Exception
  {
    if (gntmlInstance.comp(Gntml.GNTML).isEmpty()) {
      return;
    }

    PersistentRecord node = gntmlInstance.comp(Gntml.NODE);
    PersistentRecord regionStyle = gntmlInstance.comp(Gntml.REGIONSTYLE);
    PersistentRecord gntmlStyle = gntmlInstance.comp(Gntml.STYLE);

    String gntml = gntmlInstance.comp(Gntml.GNTML);

    SettableMap params = new SettableMap();
    params.set("gntml", gntml);

    gntmlCC.updateTransientInstance(node,
                                    regionStyle,
                                    gntmlInstance,
                                    gntmlStyle,
                                    transaction,
                                    params,
                                    null,
                                    null);
  }

  // --------------------------------------------------------------------------

  interface Handler
  {
    public void run(Transaction transaction,
                    WritableRecord gntmlInstance,
                    Gntml gntmlCC)
        throws Exception;
  }

  private static void withGntml(Db db,
                                String transactionPassword,
                                Gntml gntmlCC,
                                Handler handler)
      throws Exception
  {
    Table gntmlTable = gntmlCC.getInstanceTable();

    Transaction t = null;
    try {
      t = db.createTransaction(Db.DEFAULT_TIMEOUT,
                               Transaction.TRANSACTION_UPDATE,
                               transactionPassword,
                               "publish node");

      for (PersistentRecord gntmlInstance : gntmlTable) {
        handler.run(t, t.edit(gntmlInstance), gntmlCC);
      }

      t.commit();
      t = null;
    } finally {
      if (t != null)
        t.cancel();
    }
  }

  // ==========================================================================
  // Public interface

  public static final String LIPSUM_1 =
      "Aliquam erat volutpat. Mauris arcu lacus, elementum luctus, interdum eleifend, gravida in, massa. Duis nunc.";
  public static final String LIPSUM_2 =
      "Vivamus eget elit a orci semper pharetra. Sed mauris lectus, convallis nec, fringilla ut, aliquet eget, orci. Donec ligula lectus, vehicula laoreet, rhoncus ut, blandit vitae, pede. In faucibus mollis justo. Cras nec tortor. Maecenas a tortor eu tortor imperdiet dictum. Phasellus mi. Fusce urna. Integer ut quam. Vivamus sed ligula at nunc convallis tincidunt. Donec rhoncus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse tempor, libero eget ullamcorper condimentum, massa augue malesuada tortor, vitae mattis justo dui sit amet mi. Proin pretium ultrices augue.";
  public static final String LIPSUM = LIPSUM_1 + LIPSUM_2;

  /**
   * Populates every empty GNTML editable region with content. If content is null, uses a "Lorem
   * Ipsum" fragment.
   */
  public static void populateDummyGntml(Db db,
                                        String transactionPassword,
                                        Gntml gntmlCC,
                                        final String content)
      throws Exception
  {
    withGntml(db, transactionPassword, gntmlCC, new Handler() {
      @Override
      public void run(Transaction transaction,
                      WritableRecord gntmlInstance,
                      Gntml gntmlCC)
          throws Exception
      {
        populateDummyGntml(transaction,
                           transaction.edit(gntmlInstance),
                           gntmlCC,
                           content == null ? LIPSUM : content);
        WritableRecord node = transaction.edit(gntmlInstance.comp(Gntml.NODE));
        node.setField(Node.DESCRIPTION, LIPSUM_1);
      }
    });
  }

  public static void regenerateGntml(Db db,
                                     String transactionPassword,
                                     Gntml gntmlCC)
      throws Exception
  {
    withGntml(db, transactionPassword, gntmlCC, new Handler() {
      @Override
      public void run(Transaction transaction,
                      WritableRecord gntmlInstance,
                      Gntml gntmlCC)
          throws Exception
      {
        regenerateGntml(transaction, transaction.edit(gntmlInstance), gntmlCC);
      }
    });
  }

}
