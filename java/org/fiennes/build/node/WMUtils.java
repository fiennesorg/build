package org.fiennes.build.node;

import java.util.Map;
import java.util.Properties;

import org.webmacro.Broker;
import org.webmacro.Context;
import org.webmacro.InitException;
import org.webmacro.PropertyException;
import org.webmacro.engine.StringTemplate;

public class WMUtils
{
  /**
   * Evaluate template with the optional supplied context. The optional properties should be the
   * properties to use with webmacro.
   */
  public static String eval(String template,
                            Map<?, ?> contextValues,
                            Properties properties)
      throws InitException, PropertyException
  {
    if (properties == null) {
      properties = new Properties();
    }

    Broker broker = Broker.getBroker(properties);
    Context context_ = new Context(broker);
    if (contextValues != null)
      context_.getMap().putAll(contextValues);

    return (new StringTemplate(broker, template)).evaluateAsString(context_);
  }
}
