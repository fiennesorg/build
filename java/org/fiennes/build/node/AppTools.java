package org.fiennes.build.node;

import java.io.File;

import org.cord.mirror.Db;
import org.cord.mirror.Transaction;
import org.cord.node.NodeManager;
import org.cord.node.NodeManagerFactory;
import org.cord.node.config.NodeConfig;
import org.cord.util.DebugConstants;

public class AppTools
{
  private static NodeManagerFactory createAppManager(String className)
      throws ClassNotFoundException, InstantiationException, IllegalAccessException
  {
    Class<?> appManagerClass = Class.forName(className);
    return (NodeManagerFactory) appManagerClass.newInstance();
  }

  // ==========================================================================

  public interface Handler
  {
    public void run(NodeManager nodeManager,
                    String transactionPassword)
        throws Exception;
  };

  /**
   * Execute-around method that creates a nodeManager and passes it to the specified handler, making
   * sure that the nodeManager is shutdown before returning.
   */
  public static void withNodeManager(String appManagerClassName,
                                     File docBase,
                                     boolean isBooted,
                                     Handler preLockHandler,
                                     Handler postLockHandler)
      throws Exception
  {
    String transactionPassword = Db.createTransactionPassword();
    NodeManager nodeManager =
        createAppManager(appManagerClassName).createNodeManager(transactionPassword,
                                                                new NodeConfig(docBase,
                                                                               DebugConstants.DEBUG_OUT),
                                                                isBooted);
    try {
      nodeManager.getDb().lockConnectionToThread();
      if (preLockHandler != null) {
        preLockHandler.run(nodeManager, transactionPassword);
      }
      nodeManager.lock();
      if (postLockHandler != null) {
        postLockHandler.run(nodeManager, transactionPassword);
      }
    } finally {
      nodeManager.getDb().releaseConnectionFromThread();
      nodeManager.shutdown();
    }
  }

  public static void withNodeManager(String appManagerClassName,
                                     File docBase,
                                     boolean isBooted,
                                     Handler postLockHandler)
      throws Exception
  {
    withNodeManager(appManagerClassName, docBase, isBooted, null, postLockHandler);
  }

  // ==========================================================================

  public interface TransactionRunner
  {
    public void run(Transaction t)
        throws Exception;
  };

  /**
   * Execute-around method that creates a transaction and passes it to the specified handler, making
   * sure that the transaction is shutdown before returning.
   */
  public static void withTransaction(Db db,
                                     String transactionPassword,
                                     String details,
                                     TransactionRunner runner)
      throws Exception
  {
    withTransaction(db, Transaction.TRANSACTION_UPDATE, transactionPassword, details, runner);
  }

  public static void withTransaction(Db db,
                                     int transactionType,
                                     String transactionPassword,
                                     String details,
                                     TransactionRunner runner)
      throws Exception
  {
    Transaction t = null;
    try {
      t = db.createTransaction(Db.DEFAULT_TIMEOUT, transactionType, transactionPassword, details);

      runner.run(t);

      t.commit();
      t = null;
    } finally {
      if (t != null)
        t.cancel();
    }
  }

  // ==========================================================================

}
