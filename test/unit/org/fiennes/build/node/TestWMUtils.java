package org.fiennes.build.node;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.fiennes.build.node.WMUtils;
import org.testng.annotations.Test;

public class TestWMUtils
{
  private static Map<Object, Object> context(Object ...args)
  {
    HashMap<Object, Object> map = new HashMap<Object, Object>();
    
    for (int i = 0; i < args.length; i += 2)
      map.put(args[i], args[i + 1]);

    return map;
  }

  @Test
  public void plainStringEvaluatesAsItself() throws Exception {
    assertEquals(WMUtils.eval("foo", null, null), "foo");
  }

  @Test
  public void referencesAreExpanded() throws Exception {
    assertEquals(WMUtils.eval("$(foo) bar", context("foo", 1), null), "1 bar");
  }
}

